import * as THREE from './three.module.js';
export class Asteroid {
    constructor(radius) {
        // Building a sphere
        this.geometry = new THREE.DodecahedronGeometry(radius, 0);
        this.texture = new THREE.TextureLoader().load(`../resources/asteroid.jpg` );
        this.material = new THREE.MeshPhongMaterial({map: this.texture});
        this.shape = new THREE.Mesh(this.geometry, this.material);
    }

    setPosition(position) {
        this.shape.position.set(position[0], position[1], position[2]);
    }

    rotate(velocity) {
        this.shape.rotation.x += velocity[0];
        this.shape.rotation.y += velocity[1];
        this.shape.rotation.z += velocity[2];
    }
};