import * as THREE from './three.module.js';

export class Planet {

    // Planet constructor
    constructor(radius, texture, ring = false, material = 'phong') {
        // Building a sphere
        this.radius = radius;
        this.geometry = new THREE.SphereGeometry(radius, 32, 32);
        this.texture = new THREE.TextureLoader().load(`../resources/${texture}.jpg` );
        if(material == 'phong')
            this.material = new THREE.MeshPhongMaterial({map: this.texture});
        else if(material == 'basic') 
            this.material = new THREE.MeshBasicMaterial({map: this.texture});
        this.shape = new THREE.Mesh(this.geometry, this.material);
        
        if(ring) { 
            this.ring = true;
            this.ringGeometry = new THREE.RingGeometry(radius + radius / 2, radius + radius, 32);
            this.ringTexture = new THREE.TextureLoader().load(`../resources/ring.jpg` );
            this.ringMaterial = new THREE.MeshPhongMaterial({map: this.ringTexture, side: THREE.DoubleSide});
            this.ringMesh = new THREE.Mesh(this.ringGeometry, this.ringMaterial);
            this.ringMesh.rotation.x = 1.6;
        }
        // Maybe other properties

        this.distanceToSun = null;
        this.theta = 0;
        this.satellite = null;
        this.distanceToPlanet = null;
    }
    getPosition(){
        let positionx = this.shape.position.x;
        if(positionx < 0)
            positionx -= this.radius*10;
        else
            positionx += this.radius*10;

        let positiony = this.shape.position.y;
        if(positiony < 0)
            positiony -= this.radius*2;
        else
            positiony += this.radius*2;

        let positionz = this.shape.position.z;
        if(positionz < 0)
            positionz -= this.radius*10;
        else
            positionz += this.radius*10;

        let position = [positionx, positiony, positionz];
        return position;
    }
    getRealPosition(){
        let positionx = this.shape.position.x;
        let positiony = this.shape.position.y;
        let positionz = this.shape.position.z;
        let position = [positionx, positiony, positionz];
        return position;
    }
    setPosition(position) {
        this.shape.position.set(position[0], position[1], position[2]);
        if(this.ring)
            this.ringMesh.position.set(position[0], position[1], position[2]);
        this.distanceToSun = position[0];
    }

    rotate(velocity) {
        this.shape.rotation.x += velocity[0];
        this.shape.rotation.y += velocity[1];
        this.shape.rotation.z += velocity[2];

        if(this.ring) {
            this.ringMesh.rotation.z -= velocity[1];
        }
    }

    addSatellite(radius, texture, distanceToPlanet){
        this.satellite = new Planet(radius, texture);
        let position = this.getRealPosition();
        position[0] += distanceToPlanet;
        this.satellite.setPosition(position);
        this.satellite.distanceToPlanet = distanceToPlanet;
    }
    moveSatellite(dTheta){
        let position = this.getRealPosition();
        position[0] +=  this.satellite.distanceToPlanet * Math.cos(this.satellite.theta) ;
        position[2] +=  this.satellite.distanceToPlanet * Math.sin(this.satellite.theta) ;
        this.satellite.shape.position.x = position[0];
        this.satellite.shape.position.z = position[2];
        this.satellite.theta += dTheta;

    }

    move(dTheta, satelliteMovement = 0) {
        this.shape.position.x = this.distanceToSun * Math.cos(this.theta);
        this.shape.position.z = this.distanceToSun * Math.sin(this.theta);

        if(this.ring) {
            this.ringMesh.position.x = this.shape.position.x;
            this.ringMesh.position.y = this.shape.position.y;
            this.ringMesh.position.z = this.shape.position.z;
        }
        if(this.satellite){
            this.moveSatellite(satelliteMovement );
        }
        this.theta += dTheta;
    }
};