import { GUI } from './dat.gui.module.js';
import {World} from "./world.js";
import {Planet} from "./planet.js";
import {Orbit} from "./orbit.js";
import {Asteroid} from "./asteroid.js";
// World Engine
const world = new World();

// Planets
const sun = new Planet(60.03, 'sun', false, 'basic');
const mercury = new Planet(0.21, 'mercury');
const venus = new Planet(0.52, 'venus');
const earth = new Planet(0.55, 'earth');
earth.addSatellite(0.15, 'moon', 0.88);
const mars = new Planet(0.29, 'mars',);
const jupiter = new Planet(6.16, 'jupiter');
const saturn = new Planet(5.20, 'saturn', true);
const uranus = new Planet(2.20, 'uranus');
const neptune = new Planet(2.13, 'neptune');
const pluto = new Planet(0.10, 'pluto');
// Asteroids
const asteroid = new Asteroid(1.1);

// Orbits
const mercuryOrbit = new Orbit(66.03);
const venusOrbit = new Orbit(71.37);
const earthOrbit = new Orbit(75.54);
const marsOrbit = new Orbit(83.11);
const jupiterOrbit = new Orbit(138.48);
const saturnOrbit = new Orbit(203.44);
const uranusOrbit = new Orbit(347.35);
const neptuneOrbit = new Orbit(510.67);
const plutoOrbit = new Orbit(651.59);

// Movements
const movement = -1 * 2 * Math.PI;
var time = {velocity: 0};
////////////////////////MENU/////////////////////////////
//time slider
const timeGUI = new GUI();
timeGUI.add(time, 'velocity', 0, 1000, 1 );
//Select an object
const planetsGui = new GUI();
var cameraPerspectiveOn = "DefaultCamera";
var planetsGuiHelper = {
    DefaultCamera: function(){
        cameraPerspectiveOn = "DefaultCamera";
    },
    Sun: function() {
        world.setCameraPosition(sun.getPosition());
        world.controls.update();
        cameraPerspectiveOn = "DefaultCamera";
    },
    Mercury: function() {
        world.setCameraPosition(mercury.getPosition());
        world.controls.update();
        cameraPerspectiveOn = "Mercury";
    },
    Venus: function() {
        world.setCameraPosition(venus.getPosition());
        world.controls.update();
        cameraPerspectiveOn = "Venus";
    },
    Earth: function() {
        world.setCameraPosition(earth.getPosition());
        world.controls.update();
        cameraPerspectiveOn = "Earth";
    },
    Mars: function() {
        world.setCameraPosition(mars.getPosition());
        world.controls.update();
        cameraPerspectiveOn = "Mars";
    },
    Jupiter: function() {
        world.setCameraPosition(jupiter.getPosition());
        world.controls.update();
        cameraPerspectiveOn = "Jupiter";
    },
    Saturn: function() {
        world.setCameraPosition(saturn.getPosition());
        world.controls.update();
        cameraPerspectiveOn = "Saturn";
    },
    Uranus: function() {
        world.setCameraPosition(uranus.getPosition());
        world.controls.update();
        cameraPerspectiveOn = "Uranus";
    },
    Neptune: function() {
        world.setCameraPosition(neptune.getPosition());
        world.controls.update();
        cameraPerspectiveOn = "Neptune";
    },
    Pluto: function() {
        world.setCameraPosition(pluto.getPosition());
        world.controls.update();
        cameraPerspectiveOn = "Pluto";
    }
  };
planetsGui.add(planetsGuiHelper, "Sun").name("Sun");
planetsGui.add(planetsGuiHelper, "Mercury").name("Mercury");
planetsGui.add(planetsGuiHelper, "Venus").name("Venus");
planetsGui.add(planetsGuiHelper, "Earth").name("Earth");
planetsGui.add(planetsGuiHelper, "Mars").name("Mars");
planetsGui.add(planetsGuiHelper, "Jupiter").name("Jupiter");
planetsGui.add(planetsGuiHelper, "Saturn").name("Saturn");
planetsGui.add(planetsGuiHelper, "Uranus").name("Uranus");
planetsGui.add(planetsGuiHelper, "Neptune").name("Neptune");
planetsGui.add(planetsGuiHelper, "Pluto").name("Pluto");
planetsGui.add(planetsGuiHelper, "DefaultCamera").name("UnFollow");
//////////////////////////////////////////////////////

// Basically the game loop
function animate() {
    requestAnimationFrame(animate);
    // Rotation movement
    sun.rotate([0, 0.00037 * time['velocity'], 0]);
    mercury.rotate([0, 0.00017 * time['velocity'], 0]);
    venus.rotate([0, -0.00004 * time['velocity'], 0]);
    earth.rotate([0, 0.01 * time['velocity'], 0]);
    earth.satellite.rotate([0, 0.00034 * time['velocity'], 0]);
    mars.rotate([0, 0.0099 * time['velocity'], 0]);
    jupiter.rotate([0, 0.024 * time['velocity'], 0]);
    saturn.rotate([0, 0.023 * time['velocity'], 0]);
    uranus.rotate([0, 0.013 * time['velocity'], 0]);
    saturn.rotate([0, 0.015 * time['velocity'], 0]);
    pluto.rotate([0, -0.0016 * time['velocity'], 0]);

    // Traslation movement
    mercury.move(movement * 0.000114 * time['velocity']);
    earth.move(movement * 0.000027 * time['velocity'], movement * 0.00034 * time['velocity']);
    venus.move(movement * 0.000045 * time['velocity'])
    mars.move(movement * 0.000015 * time['velocity']);
    jupiter.move(movement * 0.0000023 * time['velocity']);
    saturn.move(movement * 0.0000009 * time['velocity']);
    uranus.move(movement * 0.0000003 * time['velocity']);
    neptune.move(movement * 0.0000002 * time['velocity']);
    pluto.move(movement * 0.0000001 * time['velocity']);

    planetsGuiHelper[cameraPerspectiveOn]();    
    asteroids.forEach(asteroid => {
        asteroid.rotate([getRandom(0.01, 0.1) * time['velocity'], getRandom(0.01, 0.1) * time['velocity'], getRandom(0.01, 0.1) * time['velocity']])
    });
    
    world.renderer.render(world.scene, world.camera);
}

// Making responsive window
function onWindowResize() {
    world.camera.aspect = window.innerWidth / window.innerHeight;
    world.camera.updateProjectionMatrix();
    world.renderer.setSize(window.innerWidth, window.innerHeight);
}

// Random
function getRandom(min, max) {
    return Math.random() * (max - min) + min;
}

// Setting position of planets
sun.setPosition([0, 0, 0])
mercury.setPosition([66.03, 0, 0]);
venus.setPosition([71.37, 0, 0]);
earth.setPosition([75.54, 0, 0]);
mars.setPosition([83.11, 0, 0]);
jupiter.setPosition([138.48, 0, 0]);
saturn.setPosition([203.44, 0, 0]);
uranus.setPosition([347.35, 0, 0]);
neptune.setPosition([510.67, 0, 0]);
pluto.setPosition([651.59, 0, 0]);

var asteroids = [];
var dTheta = 10 * Math.PI / 1000;
var theta = 1;

for(let i = 0; i < 800; ++i) {
    asteroids.push(new Asteroid(0.15));
    let random = getRandom(0.9, 1);
    var x = random * 110 * Math.cos(theta);
    var z = random * 110 * Math.sin(theta);
    asteroids[i].setPosition([x, 0, z]);
    theta += dTheta;
}

// Initialize a new World
world.init();
world.addToWorld(sun);
world.addToWorld(mercury);
world.addToWorld(venus);
world.addToWorld(earth);
world.addToWorld(earth.satellite);
world.addToWorld(mars);
world.addToWorld(jupiter);
world.addToWorld(saturn);
world.addToWorld(uranus);
world.addToWorld(neptune);
world.addToWorld(pluto);

world.addToWorld(mercuryOrbit);
world.addToWorld(venusOrbit);
world.addToWorld(earthOrbit);
world.addToWorld(marsOrbit);
world.addToWorld(jupiterOrbit);
world.addToWorld(saturnOrbit);
world.addToWorld(uranusOrbit);
world.addToWorld(neptuneOrbit);
world.addToWorld(plutoOrbit);

asteroids.forEach(asteroid => {
    world.addToWorld(asteroid);
});

// Starting the game loop 
animate();

// Event Listener
window.addEventListener('resize', onWindowResize, false);

