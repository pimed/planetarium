import * as THREE from './three.module.js';
export class Orbit {
    constructor(radius) {
        this.geometry = new THREE.RingGeometry(radius, radius + 0.05, 50);
        this.material = new THREE.MeshBasicMaterial({color: 0xffffff, side: THREE.DoubleSide});
        this.shape = new THREE.Mesh(this.geometry, this.material );
        const vector = new THREE.Vector3(0, 1, 0);
        this.shape.lookAt(vector);
    }

};