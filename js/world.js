import * as THREE from './three.module.js';
import {AmbientLight, HemisphereLight, PointLight, DirectionalLight, SpotLight} from './Light.js';
import {OrbitControls} from './OrbitControls.js';

export class World {

    // World constructor
    constructor() {
        this.scene = null;
        this.camera = null;
        this.renderer = null;
        this.controls = null;
        this.texture = null;
        this.ambientLight = null;
        this.light = null;
    }

    // Initialize world parameters
    init() {
        // Creating a scene
        this.scene = new THREE.Scene();
        
        // Add a camera
        this.camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.01, 1000);
        this.camera.position.z = 500
        
        // Creating a renderer
        this.renderer = new THREE.WebGLRenderer({antialias: true});    
        this.renderer.setSize(window.innerWidth, window.innerHeight);
        document.body.appendChild(this.renderer.domElement);

        // Adding stars fond    
        {
            const loader = new THREE.TextureLoader();
            this.texture = loader.load(
              '../resources/space.jpg',
              () => {
                const rt = new THREE.WebGLCubeRenderTarget(this.texture.image.height);
                rt.fromEquirectangularTexture(this.renderer, this.texture);
                this.scene.background = rt.texture;
              });
        }
        
        // Adding controls
        this.controls = new OrbitControls(this.camera, this.renderer.domElement);
        //LIGTHS
        this.light = new PointLight(0xFFFFFF, 1, [0, 0, 0]);
        //this.light.setTargetPosition([0,0,0]);
        this.light.addToScene(this.scene);
        //this.light.createHelper();
        //this.light.addHelperToScene(this.scene);
        //this.light.initGUI();

        this.ambientLight = new AmbientLight(0xFFFFFF, 0.15);
        this.ambientLight.addToScene(this.scene);
        //this.ambientLight.initGUI();
    }

    // Add an entity to the world
    addToWorld(entity) {
        if(entity.ring == true)
            this.scene.add(entity.ringMesh);
        this.scene.add(entity.shape);
    }
    
    setCameraPosition(position){
        this.camera.position.x = position[0];
        this.camera.position.y = position[1];
        this.camera.position.z = position[2];
    }
};